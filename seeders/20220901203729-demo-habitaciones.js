'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('habitaciones', [{
      precio_por_noche: 80000, 
      piso: 1,
      max_personas: 1,
      tiene_cama_bebe: "no",
      tiene_ducha: "no",
      tiene_bano: "no",
      tiene_balcon: "si",
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('habitaciones', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
