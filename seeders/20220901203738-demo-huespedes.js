'use strict';

    module.exports = {
      async up (queryInterface, Sequelize) {
        return queryInterface.bulkInsert('huespedes', [{
          nombres: "elkin dario", 
          apellidos: "valencia gomez ",
          telefono: "3113241077",
          correo: "elkinvalenciagomez@hotmail.com",
          direccion: "totoyork",
          ciudad: "popyan",
          Pais: "Colombia",
          createdAt: new Date(),
          updatedAt: new Date()
        }]);
      },
    
      async down (queryInterface, Sequelize) {
        return queryInterface.bulkDelete('huespedes', null, {});
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
      }
    };
